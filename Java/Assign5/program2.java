import java.io.*;
class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int size;
		System.out.println("Enter array size:");
		size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter elements:");

		int product=1;
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());

			if(arr[i]%2==0){
				product=product*arr[i];
			}
		}
		System.out.println("Even product is:"+product);
	}
}

