import java.io.*;
class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int size;
		System.out.println("Enter array size:");
		size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter elements:");
		int sum=0;
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==1){
				sum=sum+arr[i];
			}
		}

		System.out.println("Odd sum is:"+sum);
	}
}

