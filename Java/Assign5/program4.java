import java.io.*;
class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int size;
		System.out.println("Enter array size:");
		size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter elements:");
		for(int i=0;i<size;i++){
			arr[i]=(char)br.read();
			br.skip(1);
		}
		for(int i=0;i<size;i++){
			if(arr[i]=='a' || arr[i]=='e'|| arr[i]=='i' ||arr[i]=='o'||arr[i]=='u' ){
				System.out.println((char)arr[i]);
			}
		}
	}
}

