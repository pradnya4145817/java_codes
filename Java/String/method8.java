//String length implementation
class StrLenDemo{
	int mystrLen(String str){
		int count=0;
		char arr[]=str.toCharArray();
		for(int i=0;i<arr.length;i++){
			count++;
		}

		return count;
	}

	public static void main(String[] args){
		String str1="Pradnya";
		StrLenDemo obj=new StrLenDemo();

	//	char arr[]=str1.toCharArray();

		int length=obj.mystrLen(str1);

		System.out.println("Length of str is:"+length);
	}
}
