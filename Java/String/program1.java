class StringDemo{
	public static void main(String args[]){
		String str1="Pradnya";
		String str2=new String("Pradnya");
		String str3="Pradnya";
		char str4[]={'P','r','a','d','n','y','a'};
		String str5=new String(str3);
		String str6=new String("Pradnya");

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		System.out.println(str4);
		System.out.println(str5);

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
		System.out.println(System.identityHashCode(str5));

		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
		System.out.println(str4.hashCode());
		System.out.println(str5.hashCode());
	}
}
