//Char At
import java.util.*;
class StringDemo{
	char myCharAt(String str,int x){
		char ret='1';

		char arr[]=str.toCharArray();
		if(x<arr.length){
			ret=arr[x];
		}

		return ret;

	}
	public static void main(String[] arhs){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter string:");
		String str=sc.next();
		System.out.println("Enter index of char:");
		int x=sc.nextInt();

		StringDemo obj=new StringDemo();
		char ret=obj.myCharAt(str,x);

		if(ret=='1'){
			System.out.println("Index is not present");
		}else{
			System.out.println(ret);
		}

	}
}
