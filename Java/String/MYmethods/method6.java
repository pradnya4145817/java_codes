//EqualsTo
import java.util.*;
class StringDemo{
	boolean myEqualsTo(String str1,String str2){
		char arr1[]=str1.toCharArray();
		char arr2[]=str2.toCharArray();

		int ret=0;
		
		if(arr1.length==arr2.length){
			for(int i=0;i<arr1.length;i++){
				if(arr1[i]!=arr2[i]){
					ret=arr1[i]-arr2[i];
					return false;
				}
			}

			return true;


		}

		return false;
	



	}
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);

		String str1=sc.next();
		String str2=sc.next();

		StringDemo obj=new StringDemo();
		boolean ret=obj.myEqualsTo(str1,str2);

		if(ret==true){
			System.out.println("Strings are equal");
		}else{
			System.out.println("String are not equal");
		}


	}
}
