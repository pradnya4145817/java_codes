class Demo{
	int x=10;
	Demo(){
	}
	Demo(int x){
		this.y=x;
	}
	private int y=20;
	static int z=30;
	void disp(){
		System.out.println(x);
		System.out.println(y);
		System.out.println(z);
	}
}
class client{
	public static void main(String[] args){
		Demo obj1=new Demo(35);
		Demo obj2=new Demo();

		obj1.disp();
		obj1.x=100;
		obj2.z=300;

		obj1.disp();
		obj2.disp();

	}
}

