import java.io.*;
class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int size;
		System.out.println("Enter array Size:");
		size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter array elements :");
		int even=0,odd=0;

		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				even=even+arr[i];
			}else{
				odd=odd+arr[i];
			}
		}

		System.out.println("Even sum is:"+even);
		System.out.println("odd sum is:"+odd);
	}
}

