import java.io.*;
class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int size;
		System.out.println("Enter array Size:");
		size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter array elements :");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0;i<size;i++){
			int x=arr[i];
			int sum=0;
			while(x!=0){
				int rem=x%10;
				sum=sum+rem;
				x=x/10;
			}

			if(sum%2==0){
				System.out.println(arr[i]);
			}


		}
	}
}

