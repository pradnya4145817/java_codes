class Demo{
	Demo(){
		System.out.println("In Demo Constructor");
	}

	void fun(){
		
		System.out.println("In fun");
	}

	void fun(int x){


		System.out.println("In para fun");
	}
}
class Client{
	public static void main(String[] args){
		Demo obj=new Demo();
		obj.fun();
		obj.fun(10);
	}
}
