class Demo{
	Demo(){
		System.out.println("In Constructor Demo");
	}
	void fun(){
		System.out.println("In fun-no para");
	}
	void fun(float y){
		System.out.println("In fun-float");
	}
	void fun(int x){

		System.out.println("In fun-int");
	}

	public static void main(String[] args){
		Demo obj=new Demo();
		obj.fun();
		obj.fun(20);
		obj.fun(10.1f);
	}
}
