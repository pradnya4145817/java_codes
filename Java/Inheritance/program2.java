class Parent{
	Parent(){
		System.out.println("In Parent Constructor");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("In child Constructor");
	}
}
class Client{
	public static void main(String[] args){

		Parent obj=new Parent();
		Child obj2=new Child();

		}
}
