class ThreadDemo extends Thread {
	ThreadDemo(ThreadGroup obj,String str){
		super(obj,str);

	}

	public void run(){
		System.out.println(Thread.currentThread());
		System.out.println(Thread.currentThread().getThreadGroup());
	}

}

class ThreadGrp {
	public static void main(String[] args){
		ThreadGroup obj=new ThreadGroup("C2W");
		ThreadDemo t1=new ThreadDemo(obj,"Java");
		ThreadDemo t2=new ThreadDemo(obj,"DSA");
		
		t1.start();
		t2.start();
		System.out.println(Thread.currentThread());
		System.out.println(Thread.currentThread().getThreadGroup());


	}
}
