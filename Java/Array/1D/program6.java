import java.io.*;
class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter array size:");
		int x=Integer.parseInt(br.readLine());
		int arr[]=new int[x];
		int odd=0;
		int even=0;
		System.out.println("Enter array ele:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				even=even+arr[i];
			}else{
				odd=odd+arr[i];
			}
		}

		System.out.println("Even Sum is:"+even);
		System.out.println("Odd Sum is:"+odd);

		

	}
}
