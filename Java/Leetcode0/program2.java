import java.io.*;

class TwoSum{
	int []addTarget(int arr[], int target){
		int arrRet[]=new int[2];
		int itr=0;
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length;j++){
				if(arr[i]+arr[j]==target){
					arrRet[itr]=i;
					arrRet[itr+1]=j;
					break;
				}
			}
		}
		return arrRet;
	}

	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size=Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		System.out.println("Enter Array Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Enter Target Element:");
		int target=Integer.parseInt(br.readLine());
		TwoSum obj = new TwoSum();
		int arrRet[] =obj.addTarget(arr,target);

		for(int i=0;i<arrRet.length;i++){
			System.out.println(arrRet[i]);
		}
	}
}


