import java.io.*;
class PatternDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(br.readLine());
		int x=row/2;
	
		for(int i=1;i<=row;i++){
			
			for(int j=1;j<=i;j++){
				if(i==1 || i==x+1){
					System.out.print("$ ");
				}else if(i==2 || i==x+2){
					System.out.print("@ ");
				}else if(i==3 || i==x+3){
					System.out.print("& ");
				}else{
					System.out.print("# ");
				}
			}
			System.out.println();

		}
				
			
	}
}
				



