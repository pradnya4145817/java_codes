import java.io.*;
class Demo{
	public static void main(String[] args){
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		String str=br.readLine();
		System.out.println(str);

		int x=0;

		try{
			x=Integer.parseInt(br.readLine());
		}catch(NumberFormatException obj){
			System.out.println("Enter a valid integer number ");

			x=Integer.parseInt(br.readLine());
		}

		System.out.println(x);

	}
}
