class OddDigits{
	public static void main(String args[]){
		long x=942111423;

		int count=0;

		while(x!=0){
			long rem=x%10;

			if(rem%2!=0){
				count++;
			}

			x=x/10;
		}

		System.out.println("NO of odd Digits are "+count);
	}
}

