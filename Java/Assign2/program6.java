class PrintSum{
	public static void main(String[] args){
		int x=942111423;
		int sum=0;
		int Mul=1;

		while(x!=0){

			int rem=x%10;

			if(rem%2==0){
				sum=sum+rem;
			}else{
				Mul=Mul*rem;
			}
			x=x/10;

		}

		System.out.println("Sum of even digits is"+sum);
		System.out.println("Mul of Odd digits is"+Mul);
	}
}
