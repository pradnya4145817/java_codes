class Reverse{
	public static void main(String[] args){
		long x=942111423;

		long rev=0;

		while(x!=0){

			long rem=x%10;

			rev=rev*10+rem;

			x=x/10;
		}


		System.out.println(rev);
	}
}
