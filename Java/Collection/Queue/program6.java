import java.util.*;
class Project implements Comparable{
	String Name=null;
	int TeamSize=0;
	int Duration=0;

	Project(String Name,int TeamSize,int Duration){
		this.Name=Name;
		this.TeamSize=TeamSize;
		this.Duration=Duration;
	}
	public String toString(){
		return "{"+Name+"="+TeamSize+":"+Duration+"}";
	}

	public int compareTo(Object obj){
		return this.Name.compareTo(((Project)obj).Name);
	}
}
class PDequeDemo{
	public static void main(String[] args){
		PriorityQueue pq=new PriorityQueue();
		pq.offer(new Project("Java",10,5));
		pq.offer(new Project("Python",10,10));
		pq.offer(new Project("C",15,15));
		System.out.println(pq);
	}
}
