import java.util.ArrayDeque.*;
import java.util.*;
class ArrayDQ extends ArrayDeque {
	public static final int inc(int i,int modulus){
		if(++i>=modulus) 
			i=0;
		return i;
	}
	
    public static final int dec(int i, int modulus) {
        if (--i < 0) i = modulus - 1;
        return i;
    }
}
class Demo{

	public static void main(String[] args){
	ArrayDQ AQ=new ArrayDQ();
		AQ.offer(10);
		AQ.offer(20);
		AQ.offer(30);
		AQ.offer(40);
		System.out.println(AQ);

		System.out.println(AQ.inc(1,4));
		System.out.println(AQ.dec(2,3));
		System.out.println(AQ);
	}
}
