import java.util.*;
class CricPlayer{
	String Name=null;
	int jrNo=0;
	CricPlayer(String Name , int jrNo){
		this.Name = Name;
		this.jrNo = jrNo;
	}
	public String toString(){
		return Name+":"+jrNo;
	}
}
class ArrayDemo{
	public static void main(String[] args){
		ArrayList AL = new ArrayList();

		AL.add(new CricPlayer("Virat",18));
		AL.add(new CricPlayer("Rohit",45));
		AL.add(new CricPlayer("MSD",7));
		System.out.println(AL);

		Iterator itr = AL.iterator();
		while(itr.hasNext()){
			if("Rohit".equals(itr.next()))
				itr.remove();
			System.out.println(itr.next());
		}
		System.out.println(AL);
	}
}
