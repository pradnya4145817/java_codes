import java.util.*;
class ArrayListDemo extends ArrayList{
	public static void main(String[] args){
		ArrayListDemo Al=new ArrayListDemo();

		Al.add(10);
		Al.add(20);
		Al.add(30);
		Al.add(30);
		Al.add(30);

		System.out.println(Al);

		System.out.println(Al.size());
		System.out.println(Al.contains(40));
		System.out.println(Al.indexOf(20));
		System.out.println(Al.lastIndexOf(30));

		System.out.println(Al.get(1));
		System.out.println(Al.set(1,100));
		System.out.println(Al);

		Al.add(0,50);
		System.out.println(Al);
		System.out.println(Al.remove(0));
		//Al.clear();
		System.out.println(Al);

		ArrayList Al2= new ArrayList();
		Al2.add("Pradnya");
		Al2.add("Shraddha");
		Al2.add("Anjali");

		//Al.addAll(Al2);
		Al.add(1,Al2);
		System.out.println(Al);

		Al.removeRange(1,2);
		System.out.println(Al);

		for(Collection i:Al){
			System.out.println(i);
		}




	}
}
