import java.util.*;

class ArrayListDemo extends ArrayList{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		ArrayListDemo AL = new ArrayListDemo();
		char ch ='A';
		for(int i=0;i<5;i++){
			AL.add(ch);
			 ch++;
		}
		
		AL.add(3,'Z');
		System.out.println(AL.size());
		System.out.println(AL.get(0));
		System.out.println(AL.set(0,'X'));

		System.out.println(AL);

		ArrayList AL2 = new ArrayList();
		AL2.add("Anj");
		AL2.add("Prad");
		AL2.add("Shr");

		//AL.addAll(AL2);
		AL.addAll(2,AL2);
		System.out.println(AL);

		AL.removeRange(2,5);
		System.out.println(AL);

		Iterator itr = AL.iterator();
		while(itr.hasNext()){
			if('B'.equals(itr.next()))
				itr.remove();
			System.out.println(itr.next());
		}

	}
}
