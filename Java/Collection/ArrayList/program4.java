import java.util.*;
class MyThreadDemo extends Thread{
	Vector v=null;
	MyThreadDemo(Vector v){
		this.v=v;
	}
	public void run(){
		Thread t =Thread.currentThread();
		System.out.println(t);
		v.removeElement(10);
		System.out.println(v);
	}
}
class VectorDemo{
	public static void main(String[] args){
		Vector v=new Vector();

		v.addElement(10);
		v.addElement(15);
		v.addElement(20);
		v.addElement(30);
		v.addElement(40);

		System.out.println(v);

		Thread t =Thread.currentThread();
		System.out.println(t);
		MyThreadDemo obj=new MyThreadDemo(v);
		obj.start();
		Vector v1=new Vector();
		v1.addElement("Pradnya");
		v1.addElement("Anjali");
		v1.addElement("Shraddha");
		MyThreadDemo obj2=new MyThreadDemo(v1);
		Vector v2=new Vector();
		v2.addElement("50");
		v2.addElement("60");
		MyThreadDemo obj3=new MyThreadDemo(v2);
		obj2.start();
		obj3.start();


		System.out.println(v);
	}
}
