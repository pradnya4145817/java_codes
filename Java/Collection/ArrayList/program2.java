import java.util.*;
class CricPlayer{
	int jrNo=0;
	String Name=null;

	CricPlayer(int jrNo, String Name){
		this.jrNo=jrNo;
		this.Name=Name;
	}

	public String toString(){
		return Name+":"+jrNo;
	}
}

class ArrayDemo{
	public static void main(String args[]){

		ArrayList Al=new ArrayList();

		Al.add(new CricPlayer(18,"Virat"));
		Al.add(new CricPlayer(07,"Dhoni"));
		Al.add(new CricPlayer(45,"Rohit"));


		System.out.println(Al);
	}
}
