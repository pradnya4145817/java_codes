import java.util.*;
class IteratorDemo{
	public static void main(String[] args){

		ArrayList Al = new ArrayList();

		Al.add("Kanha");
		Al.add("Rahul");
		Al.add("Ashish");

		Iterator itr = Al.iterator();

		while(itr.hasNext()){
			if("Rahul".equals(itr.next()))
				itr.remove();

			System.out.println(itr.next());
		}

		System.out.println(Al);
	}
}
