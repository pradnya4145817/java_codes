import java.util.*;
class Company implements Comparable{
	int EmpId =0;
	String Name = null;
	Company(int EmpId, String Name){
		this.EmpId = EmpId;
		this.Name = Name;
	}
	public String toString(){
		return EmpId+":"+Name;
	}
	public int compareTo(Object obj1){
		return Name.compareTo(((Company)obj1).Name);
	}
}

class Demo{
	public static void main(String[] args){
		TreeMap Tm = new TreeMap();
		Tm.put(new Company(1,"Infosys"),"Hinjewadi");
		Tm.put(new Company(2,"Incubator"),"narhe");
		Tm.put(new Company(3,"BMC"),"Yerwada");

		System.out.println(Tm);
		Set<Map.Entry> data = Tm.entrySet();
		Iterator<Map.Entry>  itr = data.iterator();

		while(itr.hasNext()){
			Map.Entry abc = itr.next();
			System.out.println(abc.getKey()+":"+abc.getValue());
			//System.out.println(itr.next());
		}
	}
}
