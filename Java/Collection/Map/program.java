import java.util.*;
class HashMapDemo{
	public static void main(String[] args){
		HashSet hs=new HashSet();
		hs.add("Kanha");
		hs.add("Ashish");
		hs.add("Rahul");
		hs.add("Badhe");

		System.out.println(hs);
		HashMap hm = new HashMap();
		hm.put("Kanha","Barclays");
		hm.put("Ashish","Infosys");
		hm.put("Rahul","BMC");
		hm.put("Badhe","C2w");
		hm.put("Rahul","BMC");
		hm.put("Rahul","C1");
		System.out.println(hm);
		System.out.println(hm.get("Rahul"));
		System.out.println(hm.keySet());
		System.out.println(hm.values());
		System.out.println(hm.entrySet());
	}
}
