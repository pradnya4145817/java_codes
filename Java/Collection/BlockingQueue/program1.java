import java.util.concurrent.*;
class BlockingQueueDemo{
	public static void main(String[] args){
		BlockingQueue bQueue = new ArrayBlockingQueue(5);
		bQueue.offer(10);
		bQueue.offer(20);
		bQueue.add(5);
		try{

			bQueue.put(50);
			bQueue.put(70);
			
		}catch(InterruptedException e){
		}

		System.out.println(bQueue);
		System.out.println(bQueue.Capacity());
	}
}
