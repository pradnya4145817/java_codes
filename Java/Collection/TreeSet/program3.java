import java.util.*;
class Movies implements Comparable{
	String MovieName=null;
	float totColl=0.0f;

	Movies(String MovieName, float totColl){
		this.MovieName=MovieName;
		this.totColl=totColl;
	}

	public String toString(){
		return "{"+MovieName +":"+totColl+"}";
	}

	
	public int compareTo(Object obj){
		return MovieName.compareTo(((Movies)obj).MovieName);

	}

}
class TreeSetDemo{
	public static void main(String[] args){
		TreeSet Ts=new TreeSet();
		Ts.add(new Movies("Gadar2",250.00f));
		Ts.add(new Movies("Jailer",200.00f));
		Ts.add(new Movies("OMG2",150.00f));
		Ts.add(new Movies("Jailer",250.00f));

		System.out.println(Ts);

	}
}

