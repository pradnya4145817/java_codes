import java.util.*;
class Employee{
	int id = 0;
	String EmpName = null;
	Employee(int id, String EmpName){
		this.id = id;
		this.EmpName=EmpName;
	}
	public String toString(){
		return "{"+EmpName +":"+id +"}";
	}
}
class SortByName implements Comparator{
	public int compare(Object obj1, Object obj2){
		return ((Employee)obj1).EmpName.compareTo(((Employee)obj2).EmpName);
	}
}
class SortById implements Comparator{
	public int compare(Object obj1, Object obj2){
		return (((Employee)obj1).id) - (((Employee)obj2).id);
	}

}
class Demo{
	public static void main(String[] args){
		ArrayList Al = new ArrayList();
		Al.add(new Employee(4,"Pradnya"));
		Al.add(new Employee(8,"Anjali"));
		Al.add(new Employee(6,"Shra"));

		System.out.println(Al);
		Collections.sort(Al,new SortByName());
		System.out.println(Al);
		Collections.sort(Al,new SortById());
		System.out.println(Al);
	}
}
