import java.util.*;
class Myclass implements Comparable{
	String str=null;
	Myclass(String str){
		this.str=str;
	}
	public int compareTo(Object obj){

		return str.compareTo(obj);
	}

	public String toString(){
		return str;
	}
}

class TreeSetDemo{
	public static void main(String[] args){
		TreeSet Ts =new TreeSet();
		Ts.add(new Myclass("Pradnya"));
		Ts.add(new Myclass("Anjali"));
		Ts.add(new Myclass("Manali"));
		Ts.add(new Myclass("Suhani"));
		Ts.add(new Myclass("Shraddha"));

		System.out.println(Ts);
	}
}
