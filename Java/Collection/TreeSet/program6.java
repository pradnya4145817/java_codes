import java.util.*;
class Employee{
	String EmpName=null;

	float sal=0.0f;

	Employee(String EmpName, float sal){
		this.EmpName=EmpName;
		this.sal=sal;
	}
	public String toString(){
		return "{"+EmpName+":"+sal+"}";
	}
}

class SortByName implements Comparator<Employee>{
	public int compare(Employee obj1,Employee obj2){
		return obj1.EmpName.compareTo(obj2.EmpName);
	}
}

class SortBySal implements Comparator<Employee>{
	public int compare(Employee obj1, Employee obj2){
		return (int)(obj1.sal - obj2.sal);
	}
}

class ArrayListSortDemo{
	public static void main(String[] args){
		ArrayList Al =new ArrayList();

		Al.add(new Employee("Kanha",200000.0f));
		Al.add(new Employee("Ashish",250000.0f));
		Al.add(new Employee("Rahul",120000.0f));
		Al.add(new Employee("Badhe",175000.0f));
		Al.add(new Employee("Shashi",300000.0f));
		System.out.println(Al);
		Collections.sort(Al,new SortByName());
		Collections.sort(Al,new SortByName());
		Collections.sort(Al,new SortBySal());
		System.out.println(Al);
	}
}

	
