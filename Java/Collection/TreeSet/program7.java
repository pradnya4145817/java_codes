import java.util.*;
class Movies{
	String Name=null;
	float ImdbRating=0.0f;
	double totColl=0.0;

	Movies(String Name,float ImdbRating, double totColl){
		this.Name=Name;
		this.ImdbRating=ImdbRating;
		this.totColl=totColl;
	}

	public String toString(){
		return "{"+Name+":"+ImdbRating+","+totColl+"}";
	
	}
}

class SortByName implements Comparator{ 
	public int compare(Object obj1, Object obj2){
		return ((Movies)obj1).Name.compareTo(((Movies)obj2).Name);
	}
}

class SortByImdb implements Comparator{
	public int compare(Object obj1,Object obj2){
		return (int)((((Movies)obj1).ImdbRating) - (((Movies)obj2).ImdbRating));
	}
}
class SortByColl implements Comparator{
	public int compare(Object obj1,Object obj2){
		return (int)((((Movies)obj1).totColl) - (((Movies)obj2).totColl));
	}
}

class ArrayListSortDemo{
	public static void main(String[] args){
		ArrayList Al = new ArrayList();
		Al.add(new Movies("Gadar2",7.7f,250.0));
		Al.add(new Movies("OMG2",9.8f,240.0));
		Al.add(new Movies("Jailer",5.0f,150.0));
		Al.add(new Movies("Ved",8.1f,200.0));

		System.out.println(Al);

	

		Collections.sort(Al,new SortByName());
		System.out.println(Al);
		Collections.sort(Al,new SortByImdb());
		System.out.println(Al);
		Collections.sort(Al,new SortByColl());
		System.out.println(Al);
	}
}


