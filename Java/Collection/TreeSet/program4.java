//Real Time Ex
import java.util.*;
import java.io.*;

class MetroStations implements Comparable{
	String StationName=null;
	float Ticket=0.0f;

	MetroStations(String StationName, float Ticket){
		this.StationName=StationName;
		this.Ticket=Ticket;
	}

	public String toString(){
		return "{"+StationName+":"+Ticket+"}";
	}

	public int compareTo(Object obj){
		return StationName.compareTo(((MetroStations)obj).StationName);

	}
	/*public int compareTo(Object obj){
		return (int)(Ticket - (((MetroStations)obj).Ticket));
	}*/
}


class TreeSetDemo{
	public static void main(String[] args){
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		TreeSet Ts =new TreeSet();
		System.out.println("Add Elements");
		for(int i=1;i<5;i++){
			try{

				System.out.println("Enter "+i+"th  stop");

				String Name=br.readLine();
				System.out.println("Enter Ticket Price");
				float Ticket=Float.parseFloat(br.readLine());
				Ts.add(new MetroStations(Name,Ticket));
			}catch(IOException e){
			}
			
		}

		System.out.println(Ts);

	
	}
}




