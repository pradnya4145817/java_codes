import java.util.*;
class TreesetDemo{
	public static void main(String[] args){
		TreeSet Ts=new TreeSet();
		Ts.add("Pradnya");
		Ts.add("Anjali");
		Ts.add("Shraddha");
		Ts.add("Manali");

		System.out.println(Ts);
		System.out.println(Ts.clone());
		System.out.println(Ts.isEmpty());
		System.out.println(Ts.contains("Manali"));
		System.out.println(Ts.add("Suhani"));
		System.out.println(Ts);
		System.out.println(Ts.pollFirst());
		System.out.println(Ts);

		System.out.println(Ts.pollLast());
		System.out.println(Ts);
		System.out.println(Ts.clone());

	}
}
