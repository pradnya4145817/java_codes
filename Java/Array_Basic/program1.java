//Search Ele
import java.io.*;
class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int size= Integer.parseInt(br.readLine());
		int[] arr = new int[size];
		System.out.println("Enter Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] =Integer.parseInt(br.readLine());
		}
		System.out.println("Enter Search  Element:");
		int SearchEle =Integer.parseInt(br.readLine());
		int index=-1;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==SearchEle){
				index=i;
				break;
			}
		}
		if(index==-1){
			System.out.println("Element is not present in array");
		}else{
			System.out.println("Element is present at index:"+index);
		}
	}
}


