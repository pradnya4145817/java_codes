//Product of max in 1st array and min in 2nd array
import java.io.*;
class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size for 1st array");
		int size1= Integer.parseInt(br.readLine());
		int[] arr1 = new int[size1];
		System.out.println("Enter Array Elements for 1st array");
		for(int i=0;i<arr1.length;i++){
			arr1[i] =Integer.parseInt(br.readLine());
		}

		System.out.println("Enter size for 2nd array");
		int size2= Integer.parseInt(br.readLine());
		int[] arr2 = new int[size2];
		System.out.println("Enter Array Elements for 2nd array");
		for(int i=0;i<arr2.length;i++){
			arr2[i] =Integer.parseInt(br.readLine());
		}
		
		int max=arr1[0];
		for(int i=0;i<arr1.length;i++){
			if(arr1[i]>max){
				max=arr1[i];
			}
		}
		
		int min=arr2[0];
		for(int i=0;i<arr2.length;i++){
			if(arr2[i]<min){
				min=arr2[i];
			}
		}

		System.out.println("Max is="+max);
		System.out.println("Min is="+min);
		int product= max*min;
		System.out.println("Product of max from 1st Array And Min from 2nd Array is ="+product);
		
											
		
	}
		
}


