//Largest Element
import java.io.*;
class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int size= Integer.parseInt(br.readLine());
		int[] arr = new int[size];
		System.out.println("Enter Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] =Integer.parseInt(br.readLine());
		}
		
		int largeEle=arr[0];
		for(int i=0;i<arr.length;i++){
			if(arr[i]>largeEle){
				largeEle=arr[i];
			}
		}
		System.out.println("Largest Element is:"+largeEle);
	}
}


