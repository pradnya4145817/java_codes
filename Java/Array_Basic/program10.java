//Maximum odd sum
import java.io.*;
class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int size= Integer.parseInt(br.readLine());
		int[] arr = new int[size];
		System.out.println("Enter Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] =Integer.parseInt(br.readLine());
		}
		int maxSum=0;
		for(int i=0;i<arr.length;i++){
			int sum=0;
			for(int j=0;j<arr.length;j++){
				sum=sum+arr[i];
				if(sum%2!=0){
					if(sum>maxSum){
					
						maxSum=sum;
					}
				}
			}
		}

		System.out.println("Max sum is = "+maxSum);
											
		
	}
		
}


