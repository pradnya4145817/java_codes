//Find Range elements in array
import java.io.*;
class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int size= Integer.parseInt(br.readLine());
		int[] arr = new int[size];
		System.out.println("Enter Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] =Integer.parseInt(br.readLine());
		}
		System.out.println("Enter start range:");
		int s=Integer.parseInt(br.readLine());
		System.out.println("Enter End range:");
		int e=Integer.parseInt(br.readLine());
		int flag=0;
		int ele=0;

		while(s<=e){
			for(int i=0;i<arr.length;i++){
				if(arr[i]==s){
					flag=1;
					break;
				}else{
					flag=0;
					ele=s;
				}
			}
			s++;
			if(flag==0){
				System.out.println("Array does not contain:"+ele);
			}
		}
		if(flag==1){

			System.out.println("Yes");
		}
	}
		
}


