//Find subarray with a given sum
import java.io.*;
class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int size= Integer.parseInt(br.readLine());
		int[] arr = new int[size];
		System.out.println("Enter Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] =Integer.parseInt(br.readLine());
		}
		System.out.println("Enter Sum : ");
		int sum = Integer.parseInt(br.readLine());
		int index1=-1;
		int index2=-1;
		
		for(int i=0;i<arr.length;i++){
			int sum1=0;
			for(int j=i;j<arr.length;j++){
				sum1 =sum1+arr[j];
				if(sum1==sum){
					index1=i;
					index2=j;

					break;
				}
			}
		}
		if(index1==-1 && index2==-1){
			System.out.println("There is no subarray with sum = "+sum);
		}else{

			System.out.println("Sum found between index "+index1 +"and"+index2);
		}
	


	
											
		
	}
		
}


