//Binary Search - recursion
import java.util.*;
class SearchDemo{
	int SearchEle(int arr[],int k, int s , int e){
	
		if(s<=e){
		    int mid = (s+e)/2;
			
		    if(arr[mid]==k){
			 return mid;
		    }else if(arr[mid]<k){
			SearchEle(arr,k,mid+1,e);
		    }else{
			SearchEle(arr,k,s,mid-1);
			
		    }
		}
		return -1;
	}
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int size = 7;
		int arr[] = new int[size];
		System.out.println("Enter Array Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]= sc.nextInt();
		}
		System.out.println("Enter search element:");
		int k = sc.nextInt();
		SearchDemo obj = new SearchDemo();
		int s= 0; int e = arr.length-1;
		int ret = obj.SearchEle(arr,k,s,e);
		if(ret==-1){
			System.out.println("Element is not found in array.");
		}else{
			System.out.println("Element found at index:"+ret);
		
		}
	}
}
