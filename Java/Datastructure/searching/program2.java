//Binary Search
import java.util.*;
class SearchDemo{
	int SearchEle(int arr[],int k){
		int s = 0;
		int e = arr.length-1;
		while(s<=e){
			int mid = (s+e)/2;
			if(arr[mid]==k){
				return mid;
			}else if(arr[mid]<k){
				s = mid+1;
			}else{
				e = mid-1;
			}
		}
		return -1;
	}
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int size = 7;
		int arr[] = new int[size];
		System.out.println("Enter Array Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]= sc.nextInt();
		}
		System.out.println("Enter search element:");
		int k = sc.nextInt();
		SearchDemo obj = new SearchDemo();
		int ret = obj.SearchEle(arr,k);
		if(ret==-1){
			System.out.println("Element is not found in array.");
		}else{
			System.out.println("Element found at index:"+ret);
		
		}
	}
}
