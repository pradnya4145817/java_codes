class RecursionDemo{
	static int y=10;
	void fun(int x){
		System.out.println(x);
		fun(--y);
	}
	public static void main(String[] args){
		RecursionDemo obj = new RecursionDemo();
		obj.fun(10);
	}
}
