class Demo{
	
	String StringRev(String str){
		if(str==null || str.length()<=1){
			return str;
		}
		return StringRev(str.substring(1) + str.charAt(0));
		
	}
	
	public static void main(String[] args){
		Demo obj = new Demo();
		String str = "Core2Web";
		String ret = obj.StringRev(str);
		System.out.println(ret);
	}
}
