class Demo{
	int printNum(int x){
	
		if(x==0){
			return 0;
		}
	
		System.out.println(x);
		printNum(--x);
	
		return 0;
		/*
		 * for(int i =x; i>=1;i--){
		 * System.out.println(i);
		 * }
		 */
	}
	
	public static void main(String[] args){
		Demo obj = new Demo();
		int x =10;
		obj.printNum(x);
	}
}
