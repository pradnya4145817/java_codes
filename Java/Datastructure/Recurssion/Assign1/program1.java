class Demo{
	int printNum(int x){
	
		if(x==0){
			return 0;
		}
	
		printNum(--x);
		System.out.println(x+1);
		return 0;
		/*
		 * for(int i =1; i<=x;i++){
		 * System.out.println(i);
		 * }
		 */
	}
	
	public static void main(String[] args){
		Demo obj = new Demo();
		int x =10;
		obj.printNum(x);
	}
}
