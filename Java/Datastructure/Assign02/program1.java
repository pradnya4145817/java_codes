//que:01 - SubArray with given sum
import java.io.*;
class ArrayDemo{
	int[] subArray(int arr[],int num){
		int val[]=new int[]{-1,-1};
		for(int i =0;i<arr.length;i++){
			int sum =0;
			for(int j=i;j<arr.length;j++){
				sum = sum+arr[j];
				if(sum==num){
				val[0]=i+1;
				val[1]=j+1;
				return val;
				}
			}
		}
		return val;
				
				
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Size:");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		ArrayDemo obj = new ArrayDemo();
		System.out.println("Enter num:");
		int num = Integer.parseInt(br.readLine());
		int ret[] = obj.subArray(arr,num);
		for(int i =0;i<ret.length;i++){
			System.out.print(ret[i]+" ");
		}
		System.out.println();
	}
}
