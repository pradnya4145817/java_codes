//que:04 -  SubArray with 0 sum
import java.io.*;
class ArrayDemo{
	int subArray(int arr[]){
		int ret=0;
		for(int i=0;i<arr.length;i++){
			int sum =0;
			for(int j=i; j<arr.length;j++){
				sum=arr[j]+sum;
				if(sum==0){
					ret=1;
					break;
				}
			}
		}
		return ret;
				
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		ArrayDemo obj = new ArrayDemo();

		int ret = obj.subArray(arr);
		if(ret==0)
			System.out.println("No");
		else
			System.out.println("Yes");
	}
}
