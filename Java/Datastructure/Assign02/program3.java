//que:03 - Largest SubArray of 0's and 1's
import java.io.*;
class ArrayDemo{
	int subArray(int arr[]){
		int len=-1;
		int maxLen=0;
		for(int i=0;i<arr.length;i++){
			int count0=0;
			int count1=0;
			
			for(int j=i; j<arr.length;j++){
				if(arr[j]==0){
					count0++;
				}else{
					count1++;
				}
				if(count0==count1)
					len=j-i+1;
				if(maxLen<len)
					maxLen = len;
			}
		}
			return maxLen;
				
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		ArrayDemo obj = new ArrayDemo();

		int ret = obj.subArray(arr);
		System.out.println(ret);
	}
}
