//que:06- Smallest subarray with sum greater than num
import java.io.*;
class ArrayDemo{
	int subArray(int arr[],int num){
	
		int minLen = Integer.MAX_VALUE;
		for(int i=0;i<arr.length;i++){
			int sum=0;
			int len=0;
			for(int j =i;j<arr.length;j++){
				sum = sum+arr[j];
				if(sum>=num)
					len=j-i+1;

				if(minLen>len)
					minLen=len;
			}
		}
		return minLen;

				
				
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Size:");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		ArrayDemo obj = new ArrayDemo();
		System.out.println("Enter num:");
		int num = Integer.parseInt(br.readLine());
		int ret = obj.subArray(arr,num);
		System.out.println("Min length:"+ret);
	}
}
