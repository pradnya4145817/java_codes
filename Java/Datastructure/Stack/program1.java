import java.util.*;
class myStack{
	int maxSize;
	int StackArr[];
	int top = -1;
	myStack(int size){
		this.StackArr= new int[size];
		this.maxSize = size;
	}
	void push(int data){
		if(top==maxSize-1){
			System.out.println("Stack Overflow");
			
		}else{
			top++;
			StackArr[top] = data;
		}
			

	}
	boolean isEmpty(){
		if(top==-1){
			return true;
		}else{
			return false;
		}
	}
	int pop(){
		if(isEmpty()){
			System.out.println("Stack Underflow");
		
		}else{
			int val = StackArr[top];
			top--;
			return val;

		}
		return -1;
	}
	int peek(){
		if(isEmpty()){
			System.out.println("Stack is Empty");
		
		}else{
			int val = StackArr[top];
			return val;
		}
		return -1;
	}
	int StaSize(){

		return top;
	}
	void PrintStack(){

		System.out.print("[");
		for(int i=0;i<=top;i++){
			System.out.print(StackArr[i]+" ");
		}
		System.out.println("]");
	}

}
class Client{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size for stack");
		int Size = sc.nextInt();
		myStack s = new myStack(Size);
		char ch;
		do{
			
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("3.peek");
			System.out.println("4.size");
			System.out.println("5.isEmpty");
			System.out.println("6.printStack");
			System.out.println("Enter your choice");
			int choice = sc.nextInt();
			switch(choice){
				case 1:{
					       System.out.println("Enter data");
					       int data = sc.nextInt();
					       s.push(data);
					       					       
					}
					break;
				case 2:{
					   int val = s.pop();
					   if(val!=-1)
					   System.out.println(val +"is Popped");
					}
				       	break;
				case 3:{
					       int val = s.peek();
					       if(val!=-1){
						       System.out.println(val);
					       }
					}
					break;
				case 4:{
					       int stackSize = s.StaSize();
					       System.out.println("Stack Size is:"+(stackSize+1));
					}
				       break;
				case 5:{
					       boolean ret = s.isEmpty();
					       if(ret){
						       System.out.println("Stack is Empty");
					       }else{
						       System.out.println("Stack is not Empty");
					       }
					}
				       break;
				case 6:{
					       s.PrintStack();
					}
				       break;
				default:
				       System.out.println("Wrong input");
				       break;

				// System.out.println("Do you want to continue?");
				 //char ch = sc.next().charAt(0);


			}
				 System.out.println("Do you want to continue?");
				  ch = sc.next().charAt(0);
		}while(ch=='Y' || ch=='y');

	}
}

