import java.util.*;
class Sum{
	int SumofMaxMin(int arr[]){
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		for(int i=0;i<arr.length;i++){
			if(arr[i]<min){
				min=arr[i];
			}
			if(arr[i]>max){
				max=arr[i];
			}
		}
		System.out.println("min"+min);
		System.out.println("max"+max);
		int sum = max + min;
		return sum;
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Array Size:");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter Array Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		Sum obj =new Sum();
		int ret = obj.SumofMaxMin(arr);

		System.out.println("Sum is ="+ret);
	}
}
