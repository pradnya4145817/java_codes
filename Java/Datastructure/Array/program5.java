//reverse array
class ArrayDemo{
	public static void main(String[] args){
		int N=10;
		int arr[] = new int[]{2,5,1,4,8,0,8,1,3,8};
		int s=0;
		int e=arr.length-1;

		for(int i=0;i<arr.length/2;i++){
			if(s<e){
				int temp=arr[s];
				arr[s]=arr[e];
				arr[e]=temp;

				s++;
				e--;
			}
		}

		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
	}
}
