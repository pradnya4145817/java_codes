//Que:10
import java.util.*;
class ArrayDemo{
	int Possible_SubArray(int arr[]){
		int count =0;
		for(int i =0;i<arr.length;i++){
			for(int j =i; j<arr.length;j++){
				count ++;
			}
		}
		return count ;
	}
			
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size: ");
		int size=sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter Array Elements:");

		for(int i=0;i<arr.length;i++){
			arr[i]= sc.nextInt();
		}
	
		ArrayDemo obj =new ArrayDemo();
		int ret = obj.Possible_SubArray(arr);

		System.out.println("No of possible subarray wil be = "+ret);
	}
}
