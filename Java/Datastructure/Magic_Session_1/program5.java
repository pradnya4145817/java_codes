//Que:5
import java.util.*;
class ArrayDemo{
	int missEle(int arr[]){
		int min=Integer.MAX_VALUE;
		int max=Integer.MIN_VALUE;
		for(int i=0;i<arr.length;i++){
			if(arr[i]<min){
				min = arr[i];
			}
			if(arr[i]>max){
				max = arr[i];
			}
		}

		for(int i =min ; i<=max ; i++){
			int flag =0;
			for(int j =0;j<arr.length;j++){
				if(i== arr[j]){
					flag =1;
				}
			}
			if(flag==0){
				return i ;
			}
		}

		return 0;
			
	}
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size: ");
		int size=sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter Array Elements:");

		for(int i=0;i<arr.length;i++){
			arr[i]= sc.nextInt();
		}
	
		ArrayDemo obj =new ArrayDemo();
		int ret = obj.missEle(arr);
		if(ret ==0){
			System.out.println("All elements are present ");

		}else{

			System.out.println("Missing Element is "+ret);
		}
		
	}
}
