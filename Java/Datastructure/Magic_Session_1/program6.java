//Que:6

import java.util.*;
class ArrayDemo{
	int EquiEle(int arr[]){
		for(int i=0;i<arr.length;i++){
			int leftSum = 0;
			int RightSum =0;
			for(int j =0 ; j<i ; j++){
				leftSum = leftSum + arr[j];
			}

			for(int j =i+1; j<arr.length;j++){
				RightSum = RightSum + arr[j];
			}

			if(leftSum == RightSum ){
				return i;
			}
		}

		return -1;
		
			
	}
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size: ");
		int size=sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter Array Elements:");

		for(int i=0;i<arr.length;i++){
			arr[i]= sc.nextInt();
		}
	
		ArrayDemo obj =new ArrayDemo();
		int ret = obj.EquiEle(arr);

		System.out.println("Equillibrium Element is at index "+ret);
	
		
	}
}
