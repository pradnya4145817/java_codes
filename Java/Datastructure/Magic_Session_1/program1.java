//Que:1
import java.util.*;
class ArrayDemo{
	int minSum(int arr[]){
		int SumMin=Integer.MAX_VALUE;
		for(int i=0;i<arr.length;i++){
			int sum=0;
			for(int j=i;j<arr.length;j++){
				sum = sum+arr[j];


				if(sum<SumMin){
					SumMin = sum;
				}
			}
		}
		return SumMin;
	}
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size: ");
		int size=sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter Array Elements:");

		for(int i=0;i<arr.length;i++){
			arr[i]= sc.nextInt();
		}
		ArrayDemo obj =new ArrayDemo();
		int ret = obj.minSum(arr);
		System.out.println("Min Sum is ="+ret);
	}
}
