//Que:7

import java.util.*;
class ArrayDemo{
	int maxProduct(int arr[]){
		int product = 1;
		for(int i=0;i<arr.length;i++){
			int maxP = 1;
			for(int j =i ; j<arr.length ; j++){
				maxP = arr[j] * maxP;

				if(maxP> product){
					product = maxP;
				}
			}
		}
		return product;
			
		
			
	}
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size: ");
		int size=sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter Array Elements:");

		for(int i=0;i<arr.length;i++){
			arr[i]= sc.nextInt();
		}
	
		ArrayDemo obj =new ArrayDemo();
		int ret = obj.maxProduct(arr);

		System.out.println("Maximum product = "+ret);
	
		
	}
}
